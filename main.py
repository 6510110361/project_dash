import sys
import pandas as pd
import numpy as np
from pycaret.classification import *
import plotly.graph_objs as go
import plotly.express as px
import dash
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output

##pip install pandas numpy pycaret plotly dash dash-html-components dash-core-components dash-bootstrap-components


np.random.seed(42)
x = np.random.normal(size=100)
y = np.random.normal(size=100)

# ประกาศตัวแปร app ของ dash
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

# สร้าง navbar
navbar = dbc.NavbarSimple(
    brand="Grade Retire",
    brand_href="#",
    children=[
        dbc.NavItem(dbc.NavLink("Home", href="/")),
        dbc.NavItem(dbc.NavLink("Dashboard", href="/dashboard")),
    ],
    sticky="top",
)

# ประกาศ Layout แรก
app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    navbar,
    html.Div(id='page-content'),
])

# ประกาศ callback ตาม pathname ถ้า /dashboard ให้ส่งไป dashboard_layout ถ้า / ส่ง grade_layout
@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname'),],
              prevent_initial_call=True,
              suppress_callback_exceptions=True)

def display_page(pathname):
    if pathname == '/dashboard':
        return dashboard_layout
    else:
        return grade_layout
    
    
# ประกาศ ให้อ่านข้อมูลจาก excel ไฟล์ data_dropout_59-64.xlsx
df = pd.read_excel('data_dropout_59-64.xlsx')

#ทำการกำหนด column 
cols_to_replace = ['เกรดปี1เทอม1', 'เกรดปี1เทอม2', 'เกรดปี2เทอม1', 'เกรดปี2เทอม2', 'เกรดปี3เทอม1', 'เกรดปี3เทอม2', 'เกรดปี4เทอม1', 'เกรดปี4เทอม2']

#ทำการเช็ค ถ้า cols_to_replace มีค่า null ให้ใส่ 0 เข้าไปแทน
df[cols_to_replace] = df[cols_to_replace].fillna(0)

#ประกาศ columns ใหม่ GPA แล้ว นำ columns เกรด 8 เทอม ทำเป็น GPA
df['gpa'] = (df[cols_to_replace].sum(axis=1)) / len(cols_to_replace)
df = df.assign(gpa=df[cols_to_replace].mean(axis=1))

#ประกาศ เกณฑ์ ผ่าน
threshold = 1.0
#ประกาศ good_gpa ถ้าใคร gpa >= 1 ให้ ผ่าน
df = df.assign(good_gpa=(df['gpa'] >= threshold).astype(int))

#เก็บตัวแปรที่ให้แสดงใน df
keep = ['gpa', 'good_gpa', 'SEX_NAME_THAI', 'PARENTS_MARRIED_NAME','เกรดปี1เทอม1', 'เกรดปี1เทอม2', 'เกรดปี2เทอม1', 'เกรดปี2เทอม2', 'เกรดปี3เทอม1', 'เกรดปี3เทอม2', 'เกรดปี4เทอม1', 'เกรดปี4เทอม2']
df = df.loc[:, keep]

#ตัวแปร ผ่านหรือไม่ผ่าน
pass_df = df[df['gpa'] > 1]
fail_df = df[df['gpa'] <= 1]

# นำมานับว่า ผ่านกี่คนไม่ผ่านกี่คน
pass_count = len(pass_df)
fail_count = len(fail_df)

#ประกาศตัวแปร หาคนที่ gpa สูงสุดในหมู่เพศ
max_gpa = df.groupby('SEX_NAME_THAI')['gpa'].max()

#ประกาศตัวแปร หาเพศที่ผ่าน และไม่ผ่าน
pass_counts = df.loc[df['gpa'] >= 1, 'SEX_NAME_THAI'].value_counts()
fail_counts = df.loc[df['gpa'] < 1, 'SEX_NAME_THAI'].value_counts()
x = ['Male', 'Female']

pass_data = [pass_counts.get('ชาย', 0), pass_counts.get('หญิง', 0)]
fail_data = [fail_counts.get('ชาย', 0), fail_counts.get('หญิง', 0)]

# สร้าง Bar Charts
pass_bar = go.Bar(
    x=x,
    y=pass_data,
    name='Pass',
    marker=dict(color='#1f77b4')
)
fail_bar = go.Bar(
    x=x,
    y=fail_data,
    name='Fail',
    marker=dict(color='#d62728')
)

# สร้าง Layout ของ bar charts
layoutFailPass = go.Layout(
    title='Pass/Fail Counts by Sex',
    xaxis=dict(title='Sex'),
    yaxis=dict(title='Count'),
)


labels = ['Pass', 'Fail']
values = [pass_count, fail_count]
pie_chart = go.Pie(labels=labels, values=values)

bar_chart = go.Bar(x=max_gpa.index, y=max_gpa.values)

layout = go.Layout(title='Highest GPA by Sex',
                   xaxis=dict(title='Sex'),
                   yaxis=dict(title='GPA'))


# dashboard_layout หน้า dashboard
dashboard_layout = html.Div([
    html.Div([
        html.H1("DATA DROPOUT"),
        html.P("AiE & CoE 59-64"),
    ], className='jumbotron'),
    html.Div([
        dcc.Graph(
            id='scatter-plot',
            figure={
                'data': [
                    bar_chart
                ],
                'layout': {
                    'title': 'Highest GPA by Sex',
                    'xaxis': {'title': 'Sex'},
                    'yaxis': {'title': 'GPA'},
                    'margin': {'l': 40, 'b': 40, 't': 80, 'r': 10},
                    'hovermode': 'closest'
                }
            }
        ),
        dcc.Graph(
            id='scatter-plot',
            figure={
                'data': [
                    pie_chart
                ],
                'layout': {
                    'title': 'Pass & Failed',
                    'xaxis': {'title': 'Pass'},
                    'yaxis': {'title': 'Failed'},
                    'margin': {'l': 40, 'b': 40, 't': 80, 'r': 10},
                    'hovermode': 'closest'
                }
            }
        ),
        dcc.Graph(
            id='scatter-plot',
            figure={
                'data': [
                    pass_bar,
                    fail_bar,

                ],
                'layout': layoutFailPass
            }
        )
    ], className='container')
])

#layout  หลัก หน้า กรอกเกรด

grade_layout = dbc.Container([
    
    html.Br(),
    dbc.Row([
        dbc.Col([
            dbc.FormGroup(
                [
                    dbc.Label('เกรดปี 1 เทอม 1'),
                    dcc.Input(id='input-1', type='text', value='', className='form-control'),
                ],
                className='mb-3',
            ),
            dbc.FormGroup(
                [
                    dbc.Label('เกรดปี 1 เทอม 2'),
                    dcc.Input(id='input-2', type='text', value='', className='form-control'),
                ],
                className='mb-3',
            ),
            dbc.FormGroup(
                [
                    dbc.Label('เกรดปี 2 เทอม 1'),
                    dcc.Input(id='input-3', type='text', value='', className='form-control'),
                ],
                className='mb-3',
            ),
            dbc.FormGroup(
                [
                    dbc.Label('เกรดปี 2 เทอม 2'),
                    dcc.Input(id='input-4', type='text', value='', className='form-control'),
                ],
                className='mb-3',
            ),
        ]),
        dbc.Col([
            dbc.FormGroup(
                [
                    dbc.Label('เกรดปี 3 เทอม 1'),
                    dcc.Input(id='input-5', type='text', value='', className='form-control'),
                ],
                className='mb-3',
            ),
            dbc.FormGroup(
                [
                    dbc.Label('เกรดปี 3 เทอม 2'),
                    dcc.Input(id='input-6', type='text', value='', className='form-control'),
                ],
                className='mb-3',
            ),
            dbc.FormGroup(
                [
                    dbc.Label('เกรดปี 4 เทอม 1'),
                    dcc.Input(id='input-7', type='text', value='', className='form-control'),
                ],
                className='mb-3',
            ),
            dbc.FormGroup(
                [
                    dbc.Label('เกรดปี 4 เทอม 2'),
                    dcc.Input(id='input-8', type='text', value='', className='form-control'),
                ],
                className='mb-3',
            ),
        ]),
        html.Br(),
        
    ]),
    html.Br(),
    dbc.Col([
            dbc.Button('Submit', id='submit-button', n_clicks=0, className='btn btn-primary')
        ]),
    html.Br(),
    html.Div(id='output'),
], className='mt-4')


# callback ของ หน้า grade layout
@app.callback(
    dash.dependencies.Output('output', 'children'),
    [dash.dependencies.Input('submit-button', 'n_clicks')],
    [dash.dependencies.State('input-1', 'value'),
     dash.dependencies.State('input-2', 'value'),
     dash.dependencies.State('input-3', 'value'),
     dash.dependencies.State('input-4', 'value'),
     dash.dependencies.State('input-5', 'value'),
     dash.dependencies.State('input-6', 'value'),
     dash.dependencies.State('input-7', 'value'),
     dash.dependencies.State('input-8', 'value')],
     prevent_initial_call=True,
     suppress_callback_exceptions=True
)

#function ตรวจเช็คจาก callback ด้านบน ถ้า output มีการ อัพเดตให้ เข้า function นี้

def update_output(n_clicks, input1, input2, input3, input4, input5, input6, input7, input8):
    if n_clicks > 0:
        df = pd.read_excel('data_dropout_59-64.xlsx')
        cols_to_replace = ['เกรดปี1เทอม1', 'เกรดปี1เทอม2', 'เกรดปี2เทอม1', 'เกรดปี2เทอม2', 'เกรดปี3เทอม1', 'เกรดปี3เทอม2', 'เกรดปี4เทอม1', 'เกรดปี4เทอม2']
        df[cols_to_replace] = df[cols_to_replace].fillna(0)
        df['gpa'] = (df[cols_to_replace].sum(axis=1)) / len(cols_to_replace)
        df = df.assign(gpa=df[cols_to_replace].mean(axis=1))
        threshold = 1.0
        df = df.assign(good_gpa=(df['gpa'] >= threshold).astype(int))
        if input1 is None or len(input1) == 0:
            input1 = 0.00
            print("input1 is null or empty")
        if input2 is None or len(input2) == 0:
            input2 = 0.00
            print("input2 is null or empty")
        if input3 is None or len(input3) == 0:
            input3 = 0.00
            print("input3 is null or empty")
        if input4 is None or len(input4) == 0:
            input4 = 0.00
            print("input4 is null or empty")
        if input5 is None or len(input5) == 0:
            input5 = 0.00
            print("input5 is null or empty")
        if input6 is None or len(input6) == 0:
            input6 = 0.00
            print("input6 is null or empty")
        if input7 is None or len(input7) == 0:
            input7 = 0.00
            print("input7 is null or empty")
        if input8 is None or len(input8) == 0:
            input8 = 0.00
            print("input8 is null or empty")
        g = [round(float(input1),2), round(float(input2),2), round(float(input3),2), round(float(input4),2), round(float(input5),2), round(float(input6),2), round(float(input7),2), round(float(input8),2)]
        print(g)
        remove = (['STUDENT_ID','ADMIT_YEAR', 'ADMIT_TERM', 'MAJOR_ID', 'MAJOR_NAME_THAI',
            'DEPT_ID', 'DEPT_NAME_THAI', 'FAC_ID', 'FAC_NAME_THAI',
            'STUDY_LEVEL_ID', 'STUDY_LEVEL_NAME', 'STUDY_TYPE_ID',
            'STUDY_TYPE_NAME', 'STUDY_PLAN_NAME', 'DEGREE_ID', 'COURSE_NAME',
            'COURSE_DEGREE', 'COURSE_TYPE_NAME', 'STUDY_STATUS', 'STATUS_DESC_THAI',
            'STILL_STUDENT', 'NATIONALITY', 'SEX_SHORT_NAME_THAI', 'SEX_NAME_THAI',
            'ENT_METHOD', 'ENT_METHOD_DESC', 'STUD_BIRTH_DATE',
            'STUD_BIRTH_PROVINCE_ID', 'PROVINCE_NAME_THAI', 'BIRTH_COUNTRY',
            'COUNTRY_NAME_ENG', 'PREV_INSTITUTION_NAME', 'INSTITUTION_PROVINCE_ID',
            'INSTITUTION_PROVINCE_NAME', 'ENG_SCORE', 'CAMPUS_ID',
            'CAMPUS_NAME_THAI', 'IS_GRAD_IN_COURSE', 'CURR_REG', 'IN_PROVINCE',
            'IN_PROVINCE_CAMPUS', 'เกรดปี1เทอม3',
            'เกรดปี2เทอม3', 'เกรดปี3เทอม3',
            'เกรดปี4เทอม3', 'GPA_SCHOOL', 'FUND_NAME', 'FUND_NAME_CODE',
            'DEFORMITY_NAME', 'DEFORMITY_NAME_code','PARENTS_MARRIED_NAME',])
        df = df.drop(columns=remove)
        #print(df.columns)
        keep = ['gpa', 'good_gpa']
        df = df.loc[:, keep]
        
        result = check_grade(df,g)
        credit_hours = [1, 1, 1, 1, 1, 1, 1, 1]

        weighted_sum = sum([g[i] * credit_hours[i] for i in range(len(g))])
        total_credit_hours = sum(credit_hours)
        gpa = weighted_sum / total_credit_hours
        
        formatted_value = "{:.2f}".format(gpa) 
        
        n_clicks = 0
        
        if result == 0:
            return  html.P(f'GPA: {formatted_value} คุณมีโอกาสโดน รีไทร์')
        else:
            return  html.P(f'GPA: {formatted_value} คุณไม่โดน รีไทร์')



#function ตรวจเช็คเกรด 8 เทอมว่าผ่านไหม

def check_grade(df,g):
    setup(data=df, target='good_gpa', n_jobs=4, silent=True)

    clf = create_model('lr')
    evaluate_model(clf)
    
    grades = g
    credit_hours = [1, 1, 1, 1, 1, 1, 1, 1]

    weighted_sum = sum([grades[i] * credit_hours[i] for i in range(len(grades))])
    total_credit_hours = sum(credit_hours)
    gpa = weighted_sum / total_credit_hours

    good_gpa = 0
    if (gpa >= 1):
        good_gpa = 1
    print(gpa,good_gpa)
    
    
    new_data = pd.DataFrame({
        'grade': [g],
        'gpa': [gpa],
        'good_gpa': [good_gpa] # ข้อมูลที่กรอกมาและข้อมูลที่ train จะผ่านตรงนี้
    })
    predictions = predict_model(clf, data=new_data)
    predicted_retirement = predictions['Label'][0]
    
    print(predicted_retirement)
    return predicted_retirement




if __name__ == '__main__':
    app.run_server(debug=False)